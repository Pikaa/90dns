## Self-hosting Guide

**You don't have to self host.** You can simply use one of the IPs above and it'll have the same functionality. This is mostly for people who want to block more/less addresses or for people who have issues accessing dns servers outside of their networks due to their ISPs.

- Decide on which DNS server to use. Dnsmasq is easier to use but BIND9 is designed for bigger scale deployments, though there's some cases where dnsmasq was deployed on a big scale and did well. I prefer BIND9.
- Decide on if you'll trust me with wifi check bypass or not. If so, don't bother with getting an HTTP server.
- Currently only nginx is supported, but I plan to support apache2 too in the future (if you write a config, send a Merge Request).

- Install your preferred DNS software on a server, start and enable it (example: `apt install bind9`, `systemctl enable --now bind9`).
- Make sure that the port 53 is both open and accessible (you can use `nmap <IP>` on a different machine)

Only if you chose to not trust me with wifi check bypass:

- Install nginx on a server, start and enable it (example: `apt install nginx`, `systemctl enable --now nginx`)
- Make sure that the port 80 is both open and accessible (you can use `nmap <IP>` on a different machine)

### BIND9

This part of guide is only for people who chose to go with BIND9. Please do not follow this section if you chose to use dnsmasq, thank you!

- Download the `BIND9/named.conf.options` file from this repo, and replace `/etc/bind/named.conf.options` on the server with that file
- Optional: Change the forward DNS IPs on `/etc/bind/named.conf.options` (if you don't trust lavadns or 1.1.1.1)
- Make a directory at `/etc/bind/zones`
- Download all the files in `BIND9/zones/` folder of the repo to `/etc/bind/zones` on server
- Optional, for people who didn't trust with wifi check bypass: edit the files `/etc/bind/zones/nintendo.net` and `/etc/bind/zones/nintendowifi.net`, replace `95.216.149.205` with your own IP (if you intend to make the service accessible outside of your home network use your public IP, if not use your private IP)
- Make sure that bind user can access `/etc/bind/zones` folder (you may need to tinker with permissions here)
- Restart BIND9 (`# systemctl restart bind9`)

### dnsmasq

This part of guide is only for people who chose to go with dnsmasq. Please do not follow this section if you chose to use BIND9, thank you!

- Download the `dnsmasq/dnsmasq.conf` file from this repo, and replace `/etc/dnsmasq.conf` on the server with that file
- Optional, for people who didn't trust with wifi check bypass: edit lines with `nintendo.net` and `nintendowifi.net`, replace `95.216.149.205` with your own IP (if you intend to make the service accessible outside of your home network use your public IP, if not use your private IP)
- Restart dnsmasq (`# systemctl restart dnsmasq`)

### nginx (only if you chose to not trust me with wifi check bypass)

- Download the `nginx/switchwifi` file from the repo to your `/etc/nginx/sites-enabled` folder (on non-debian based OSes you might need to set this folder yourself by messing with `/etc/nginx/nginx.conf`)
- Reload nginx (`# systemctl reload nginx`)

### DNS (optional)

[this is the only part that I'm not too sure about, sorry. second step might not be necessary]

- Set `dns90.a3.pm.` to your own domain on all BIND9 zones
- Add an A record to that on your registrar's (or NS provider's) DNS settings, point to your DNS server